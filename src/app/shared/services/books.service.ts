import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  constructor(private http: HttpClient) {}

  fetchBooks(query: string): Observable <any> {
    const url = `http://localhost:5005/api/book/search?search=${query}`;
    return this.http.get(url);
  }
}
