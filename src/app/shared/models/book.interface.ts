export interface Book {
    title: string;
    year: number;
    genre: string;
    pageNumber: number;
    publisher: string;
    publiserdId: number;
    author: string;
    authorId: number;
}