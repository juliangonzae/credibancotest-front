import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { Book } from 'src/app/shared/models/book.interface';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  @Output() search: EventEmitter<string> = new EventEmitter<string>();

  searchText: string = '';
  books: Book[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  fetchBooks() {
    this.search.emit(this.searchText);
  }
}
