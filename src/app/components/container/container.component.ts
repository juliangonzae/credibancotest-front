import { Component, OnInit } from '@angular/core';
import { Book } from 'src/app/shared/models/book.interface';
import { BooksService } from 'src/app/shared/services/books.service';

@Component({
  selector: 'app-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css']
})
export class ContainerComponent implements OnInit {

  books: Book[] = [];
  
  constructor(private _booksService: BooksService) { }

  ngOnInit(): void {
  }
  
  handleSearch(e: any) {
    this._booksService.fetchBooks(e).subscribe(data => {
      this.books = data;
    });
  }
}
